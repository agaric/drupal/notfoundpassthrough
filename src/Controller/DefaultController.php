<?php
/**
 * @file
 * Contains \Drupal\notfoundpassthrough\Controller\DefaultController.
 */

namespace Drupal\notfoundpassthrough\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Default controller for the notfoundpassthrough module.
 */
class DefaultController extends ControllerBase implements ContainerInjectionInterface {
  /**
   * Symfony\Component\HttpKernel\HttpKernelInterface definition.
   *
   * @var Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * An instance of the "request_stack" service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  public function __construct(HttpKernelInterface $http_kernel, RequestStack $request_stack, LanguageManagerInterface $language_manager) {
    $this->httpKernel = $http_kernel;
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_kernel.basic'),
      $container->get('request_stack'),
      $container->get('language_manager')
    );
  }

  public function _notfoundpassthrough_title() {
    return $this->config('notfoundpassthrough.settings')->get('title');
  }

  public function notfoundpassthrough(Request $request) {
    $servers = explode("\n", $this->config('notfoundpassthrough.settings')->get('servers'));
    // Initialize the variables.
    $new_status = '';
    $new_status_code = 0;
    $new_path = '';
    $save_redirect = $this->config('notfoundpassthrough.settings')->get('save_redirect');
    $statuses = _notfoundpassthrough_redirect_options();
    $redirect_code = $this->config('notfoundpassthrough.settings')->get('redirect_code');
    $force_status = $this->config('notfoundpassthrough.settings')->get('force_redirect_code');
    $code = NULL;
    // Prepare protocol set to current to use for servers .
    $protocol = $GLOBALS['base_url'] == $GLOBALS['base_insecure_url'] ? 'http://' : 'https://';
    if ($request->getRequestUri() !== '/notfoundpassthrough') {
      foreach ($servers as $server) {
        // If a status code is set, our work looping through servers is done.
        if ($new_status_code) {
          break;
        }

        $path = (FALSE === strpos($server, '://')) ? $protocol . trim($server) : trim($server);
        if (FALSE === strpos($path, '[request_uri]')) {
          $path .= $request->getRequestUri();
        }
        else {
          $path = str_replace('/[request_uri]', $request->getRequestUri(), $path);
        }
        $client = \Drupal::httpClient('',
          [
            'request.options' => [
              'timeout' => 3,
              'connection_timeout' => 2,
            ],
          ]);
        try {
          $response = $client->head($path);
          $code = $response->getStatusCode();
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
          watchdog_exception('notfoundpassthrough', $e, 'Caught response: ' . $e->getResponse()->getStatusCode());
        }
        catch (\GuzzleHttp\Exception\RequestException $e) {
          watchdog_exception('notfoundpassthrough', $e);
        }
        catch (\GuzzleHttp\Exception\ServerException $e) {
          watchdog_exception('notfoundpassthrough', $e);
        }
	catch (\InvalidArgumentException $e) {
          watchdog_exception('notfoundpassthrough', $e, 'Please ensure you have configured fallback servers and that the URLs for each are valid.  %type: @message in %function (line %line of %file).');
        }
        if ($code >= 200 && $code <= 250) {
          // We found the actual document on this server, set to 302 Found.
          $new_status_code = $redirect_code;
          // TODO figure out how to get Guzzle to give us back the URL/path
          // that we finally ended up on.  There doesn't seem to be a way so
          // we would have to send the Max-Forwards request-header and collect
          // each path as we go.

          $new_path = $path;
        }
        elseif ($code >= 300 && $code <= 305) {
          // This is never going to happen...  if we decide we care...
          // TODO figure out how to get Guzzle to give us back the URL/path
          // that we finally ended up on.  There doesn't seem to be a way so
          // we would have to send the Max-Forwards request-header and collect
          // each path as we go.
          // For now, knowing that the path we started with ultimately ended up
          // with a 200 status code (captured in above) is good enough.

          // No resource, but found another redirect, so follow the redirect.
          $new_status_code = !$force_status ? $code : $redirect_code;
          // $new_path = (isset($header_info->redirect_url)) ? ($header_info->redirect_url) : $path;
        }
        // Anything other than 200s or 300s and the path wasn't found, so we
        // have nothing to redirect to.
      }
    }
    // Generate the redirect code based on redirect code.
    if ($new_status_code) {
      $new_status = $statuses[$new_status_code];
    }
    // If there is no status/path to use, see if we are searching.
    if (!$new_status && !$new_path) {
      // TODO Integrate with Search 404 instead of doing this ourselves.
      $search_path = $this->config('notfoundpassthrough.settings')->get('search');
      if ($search_path) {
        \Drupal::messenger()->addError('The page you were trying to navigate to could not be found. Below you will see search results which may help you find what you are looking for');
        // drupal_set_message(t('The page you were trying to navigate to could not be found. Below you will see search results which may help you find what you are looking for.'));
        $new_status = '302 Found';
        $new_status_code = 302;
        $new_path = $search_path . '/' . trim(str_replace('/', ' ', $request->getRequestUri()));
        // We do not save a search redirect.
        $save_redirect = FALSE;
      }
    }
    // If there is no status/path to use, see if a simple redirect is specified.
    if (!$new_status && !$new_path) {
      $redirect_path = $this->config('notfoundpassthrough.settings')->get('redirect');
      if ($redirect_path) {
        $new_status = '302 Found';
        $new_status_code = 302;
        $new_path = str_replace('/[request_uri]', $request->getRequestUri(), $redirect_path);
      }
    }
    // If there is still no status/path to use, fall back to set 404 path.
    if (!$new_status && !$new_path) {
      $redirect_path = $this->config('notfoundpassthrough.settings')->get('site_404');
      if ($redirect_path) {
        if ($this->languageManager->isMultilingual()) {
          $language = $this->languageManager->getCurrentLanguage()->getId();
          $redirect_path = '/' . $language . $redirect_path;
        }
        $sub_request = Request::create($redirect_path, 'GET');
        if ($session = $this->requestStack->getCurrentRequest()->getSession()) {
          $sub_request->setSession($session);
        }
        $sub_response = $this->httpKernel->handle($sub_request, HttpKernelInterface::SUB_REQUEST);

        return $sub_response;
      }
    }
    // If there is a status and a path, we go there.
    if ($new_status && $new_path) {
      // TODO after Redirect module gets some developer documentation
      // https://www.drupal.org/project/redirect/issues/3001965
      /*
      if (\Drupal::moduleHandler()->moduleExists('redirect') && $save_redirect) {
        // Save to Redirect module system.
        $redirect = new stdClass();
        redirect_object_prepare($redirect, [
          'source' => ltrim($request->getRequestUri(), '/'),
          'redirect' => $new_path,
          'status_code' => $new_status_code,
        ]);
        redirect_save($redirect);
      }
      */
      // Remove the destination because Drupal 8 is trying to drive its
      // developers to suicide.  There is a proposed fix in this issue:
      // https://www.drupal.org/project/drupal/issues/2950883
      \Drupal::request()->query->remove('destination');
      // TODO probably only do the above and below when actually redirecting to
      // a remote site, not doing one of the in-site redirects that's also
      // possible.
      \Drupal::logger('notfoundpassthrough')->notice('Redirected to found path ' . $new_path);
      return new TrustedRedirectResponse($new_path, $new_status_code);
    }

    $default4xxController = new \Drupal\system\Controller\Http4xxController();
    return $default4xxController->on404();
  }
}
